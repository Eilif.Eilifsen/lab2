package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    int fridgeCapacity = 20;
    ArrayList<FridgeItem> fridgeArray = new ArrayList<FridgeItem>();
    ArrayList<FridgeItem> expiredFood = new ArrayList<FridgeItem>();

    public int nItemsInFridge() {
        return fridgeArray.size();
    }

    public int totalSize() {
        return fridgeCapacity;
    }

    public boolean placeIn(FridgeItem item) {
        if (fridgeArray.size() < fridgeCapacity) {
            fridgeArray.add(item);
            return true;
        } else {
            return false;
        }
    }

    public void takeOut(FridgeItem item) {
        if (fridgeArray.contains(item)){
            fridgeArray.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }

    public void emptyFridge() {
        fridgeArray.clear();
    }

    public List<FridgeItem> removeExpiredFood() {
        for (int i=0; i<nItemsInFridge(); i++){
            if (fridgeArray.get(i).hasExpired()) {
                expiredFood.add(fridgeArray.get(i));
                takeOut(fridgeArray.get(i));
                i--;
            }
        }
        return expiredFood;
    }

}
